package Exercise_1;

import java.util.Scanner;

public class Ex_1_g {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.print("Enter count number:");
        int x = input.nextInt();

        if (x > 1) {
            System.out.print("Enter digit:");
            double digit = Math.abs(input.nextDouble());

            double sumPlus = digit;
            double sumMultiplication = digit;

            for (int i = 1; i < x; i++) {
                System.out.print("Enter digit:");
                digit = Math.abs(input.nextDouble());
                sumPlus += digit;
                sumMultiplication *= digit;
            }
            System.out.println("Result sum:" + sumPlus);
            System.out.println("Result multiplication:" + sumMultiplication);
        }
    }
}
