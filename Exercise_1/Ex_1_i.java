package Exercise_1;

import java.util.Scanner;

public class Ex_1_i {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.print("Enter count number:");
        int x = input.nextInt();

        double sum = 0;
        for (int i = 1; i <= x; i++) {
            System.out.print("Enter digit:");
            sum += (input.nextDouble() * Math.pow(-1, i)) / Factorial(i);
        }
        System.out.println("Result sum:" + sum);
    }

    private static double Factorial(double n) {
        if (n <= 2)
            return n;
        return n * Factorial(n - 1);
    }
}
