package Exercise_1;

import java.util.Scanner;

public class Ex_1_h {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.print("Enter count number:");
        int x = input.nextInt();

        double sum = 0;
        for (int i = 1; i <= x; i++) {
            System.out.print("Enter digit:");
            sum += input.nextDouble() * Math.pow(-1, i + 1);
        }
        System.out.println("Result sum:" + sum);
    }
}
