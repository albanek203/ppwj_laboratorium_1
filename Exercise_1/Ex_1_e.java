package Exercise_1;

import java.util.Scanner;

public class Ex_1_e {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.print("Enter count number:");
        int x = input.nextInt();

        if (x > 1) {
            System.out.print("Enter digit:");
            double multiplication = Math.abs(input.nextDouble());
            for (int i = 1; i < x; i++) {
                System.out.print("Enter digit:");
                multiplication *= Math.abs(input.nextDouble());
            }
            System.out.println("Result multiplication:" + multiplication);
        }
    }
}
