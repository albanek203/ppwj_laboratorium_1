package Exercise_1;

import java.util.Scanner;

public class Ex_2 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.print("Enter count number:");
        int x = input.nextInt();

        var array = new int[x];
        for (int i = 0; i < x; i++) {
            System.out.print("Enter digit:");
            array[i] = input.nextInt();
        }

        for (int i = 0; i < array.length; i++) {
            if (i != array.length - 1)
                System.out.print(array[i + 1] + " ");
            else
                System.out.print(array[0]);
        }
    }
}
