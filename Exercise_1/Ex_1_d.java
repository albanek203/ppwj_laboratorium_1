package Exercise_1;

import java.util.Scanner;

public class Ex_1_d {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.print("Enter count number:");
        int x = input.nextInt();

        double sum = 0;
        for (int i = 0; i < x; i++) {
            System.out.print("Enter digit:");
            sum += Math.sqrt(Math.abs(input.nextDouble()));
        }
        System.out.println("Result sqrt abs sum:" + sum);
    }
}
