package Exercise_2;

import java.util.Scanner;

public class Ex_1_e {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.print("Enter count digit:");
        int x = input.nextInt();

        var count = 0;
        for (int i = 1; i <= x; i++) {
            System.out.print("Enter digit:");
            var digit = input.nextInt();
            if (Math.pow(2,i) < digit && digit < Factorial(i))
                count++;
        }

        System.out.println("\nCount: " + count);
    }
    private static double Factorial(double n) {
        if (n <= 2)
            return n;
        return n * Factorial(n - 1);
    }
}
