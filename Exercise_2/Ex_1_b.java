package Exercise_2;

import java.util.Scanner;

public class Ex_1_b {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.print("Enter count digit:");
        int x = input.nextInt();

        var count = 0;
        for (int i = 0; i < x; i++) {
            System.out.print("Enter digit:");
            var digit = input.nextInt();
            if (digit % 3 == 0 && digit % 5 != 0)
                count++;
        }

        System.out.println("\nCount: " + count);
    }
}
