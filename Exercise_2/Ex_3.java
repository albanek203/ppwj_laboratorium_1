package Exercise_2;

import java.util.Scanner;

public class Ex_3 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.print("Enter count digit:");
        int x = input.nextInt();

        System.out.print("Enter digit:");
        var digit = input.nextInt();
        var maxNumber = digit;
        var minNumber = digit;
        for (int i = 0; i < x - 1; i++) {
            System.out.print("Enter digit:");
            digit = input.nextInt();
            if (digit > maxNumber)
                maxNumber = digit;
            else if (digit < minNumber)
                minNumber = digit;
        }

        System.out.println("Max: " + maxNumber);
        System.out.println("Min: " + minNumber);
    }
}
