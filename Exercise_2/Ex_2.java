package Exercise_2;

import java.util.Scanner;

public class Ex_2 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.print("Enter count digit:");
        int x = input.nextInt();

        for (int i = 1; i <= x; i++) {
            System.out.print("Enter digit:");
            var digit = input.nextInt();
            if (digit > 0)
                System.out.println("Digit: " + digit + " podwojona suma = " + digit * 2);
        }
    }
}
