package Exercise_2;

import java.util.Scanner;

public class Ex_4 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.print("Enter count digit:");
        int x = input.nextInt();


        var prevNumber = 0;
        var count = 0;
        for (int i = 0; i <= x - 1; i++) {
            System.out.print("Enter digit:");
            var digit = input.nextInt();
            if (i != 0 && digit > 0 && prevNumber > 0)
                count++;

            prevNumber = digit;
        }

        System.out.println("\nCount: " + count);
    }
}
