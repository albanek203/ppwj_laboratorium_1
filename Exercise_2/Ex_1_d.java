package Exercise_2;

import java.util.Scanner;

public class Ex_1_d {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.print("Enter count digit:");
        int x = input.nextInt();

        var prevNumber = 0;
        var count = 0;
        for (int i = 0; i < x; i++) {
            System.out.print("Enter digit:");
            var digit = input.nextInt();
            if (digit < (prevNumber + digit) / 2 && i != 0)
                count++;

            prevNumber = digit;
        }

        System.out.println("\nCount: " + count);
    }
}
